#!/usr/bin/env python3
# coding=utf-8

from aiohttp import web
import asyncio
import re
import smtplib
import uuid
import datetime
from email.mime.text import MIMEText
from subprocess import Popen, PIPE

CONTACT_EMAIL_TO = 'kontakt@freifunk-bs.de'

EMAIL_REGEX = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

template = """<meta charset="utf-8">\n%s<br>\n<a href="/kontakt.html">zurück</a>"""

async def handle(req):
    form = await req.post()
    try:
        email = form['email']
        if not re.match(EMAIL_REGEX, email):
            raise ValueError()
        message = form['message']
        
        captcha = form['captcha']
        captcha = captcha.replace("'", "")
        captcha = captcha.replace('"', "")
        captcha = captcha.strip()

        today = str(datetime.datetime.now().day)
        if today == captcha:
            msg = MIMEText(message, 'plain', 'utf-8')
            msg['From'] = "kontakt@freifunk-bs.de"
            msg['To'] = CONTACT_EMAIL_TO
            msg['Subject'] = 'Kontaktanfrage von %s' % email
            msg['Reply-To'] = ','.join([email,CONTACT_EMAIL_TO])
            ruuid = str(uuid.uuid4()) #get a random uuid
            msg['Message-ID'] = '<'+ruuid+'@freifunk-bs.de>'
            p = Popen(["/usr/sbin/sendmail", "-t", "-oi", "-FKontaktformular"], stdin=PIPE)
            p.communicate(msg.as_string())
            return web.Response(text = template % "Ihre Nachricht wurde entgegengenommen")
        else:
            msg = MIMEText(message, 'plain', 'utf-8')
            msg['From'] = "kontakt@freifunk-bs.de"
            msg['To'] = CONTACT_EMAIL_TO
            msg['Subject'] = '[CAPTCHA]Kontaktanfrage von %s' % email
            msg['Reply-To'] = ','.join([email,CONTACT_EMAIL_TO])
            ruuid = str(uuid.uuid4()) #get a random uuid
            msg['Message-ID'] = '<'+ruuid+'@freifunk-bs.de>'
            msg['X-FFBS-CAPTCHA'] = "Failed: User tried: {}; I expected: {}".format(captcha, today)
            p = Popen(["/usr/sbin/sendmail", "-t", "-oi", "-FKontaktformular"], stdin=PIPE)
            p.communicate(msg.as_string())
            return web.Response(text = template % "Der Spamschutz wurde nicht erfolgreich ausgefüllt. Bitte versuchen Sie es erneut.")
    except Exception as e:
        return web.Response(text = template % "Die Anfrage ist ungültig")

app = web.Application()
app.add_routes([web.post('/contact', handle)])

if __name__ == '__main__':
    web.run_app(app, host='::1', port=7392)

# vim: expandtab:shiftwidth=4:softtabstop=4

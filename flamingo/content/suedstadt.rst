blog:
   - date: 2021-01-23
     title: Freifunk um die Kirchen des Pfarrverbandes Braunschweiger Süden (Update 2021-01-23)
     body: Der Pressespiegel wurde um einen Link zur Ev.-luth. Propstei Braunschweig ergänzt.
   - date: 2021-01-03
     title:  Freifunk um die Kirchen des Pfarrverbandes Braunschweiger Süden
     body:   Der Pfarrverband Braunschweiger Süden hat sich zum Ziel gesetzt die Attraktivität ihrer Kirchenstandorte zu verbessern. Hierso wird an allen acht Kirchen in den sieben Gemeinden - und wo vorhanden angrenzenden Gemeinderäumen - kostenloses WLAN zur Verfügung gestellt.


Freifunk um die Kirchen des Pfarrverbandes Braunschweiger Süden
===============================================================

.. img::
   suedstadt_kirchturm.jpg
   :align: right
   :width: 400px

   **Kirchturm St. Markus (Südstadt)**
   Die Freifunk-Router sind an den Kirchen so verteilt, dass neben den
   Stationen des Pilgerwegs auch große Teile der Kirchen und Gemeindehäuser
   abgedeckt sind.

Der 
`Pfarrverband Braunschweiger Süden <https://www.pfarrverband-braunschweiger-sueden.de/>`__
hat sich zum Ziel gesetzt die Attraktivität
ihrer Kirchenstandorte zu verbessern.
Hierso wird an allen acht Kirchen in den sieben Gemeinden - und wo vorhanden in den angrenzenden
Gemeinderäumen - kostenloses WLAN zur Verfügung gestellt.

Bei der Planung und Inbetriebnahme wurden sie vom Freifunk Braunschweig unterstützt.
Die Braunschweiger Freifunker haben bei der Begehung der Standorte, sowie bei der
Planung ihre Erfahrung mit eingebracht.
Die Inbetriebnahme und Installation wurde von Angehörigen des Pfarrverbandes
selbst übernommen.
Das Projekt wurde darüber aus Mitteln der drei 
`Stadtbezirksräte <https://www.braunschweig.de/politik_verwaltung/politik/ratderstadt/stadtbezirksraete/aufgaben_stbezrat.php>`__
Stöckheim-Leiferde, Heidberg-Melverode und Südstadt-Rautheim-Mascherode gefördert.

Insgesamt wurden an den acht Standorten 29 WLAN-Accesspoints, sowie andere
Netzwerktechnik in Betrieb genommen und viele Meter Netzwerkkabel verlegt.

Somit sind die Kirchen und Gemeinden des Pfarrverbandes nun nicht nur in der
analogen Welt mit dem 
`Pilgerweg <https://www.pfarrverband-braunschweiger-sueden.de/pilgerweg/>`__,
sondern auch in der digitalen Welt vernetzt!
Für Freifunk ist diese Kombination aber aus einem weiteren Grund interessant:
Die Stationen des Pilgerwegs befinden sich jeweils im Außenbereich der Kirchen und
ein Ziel des Projektes war es, eben diese Stationen auch mit Freifunk zu versorgen.

Somit steht Freifunk an diesen Standorten nicht nur den Gemeindemitgliedern in den
Räumen des Pfarrverbandes zur Verfügung, sondern auch jeder anderen Person, die
auf der Suche nach einem offenen und freien WLAN ist.

.. img::
   suedstadt_ffbs.jpg
   :align: right
   :width: 400px

   **Ein Teil des Freifunk Braunschweig Teams**
   Da dies Projekt durch die Corona-Pandemie unter Einhaltung von Infektionsschutzmaßnahmen
   durchgeführt wurde, durften auch nur ein Teil der Braunschweiger Freifunker auf
   das Gruppenfoto 🙂

Wir Bedanken uns beim Team des Pfarrverbandes Braunschweiger Süden für die gute
Zusammenarbeit!

Sollen Sie Interesse an der Durchführung ähnlicher Projekte haben 
`sprechen Sie uns gern an </kontakt.html>`__.

Weiterführende Links
....................

* `Pilgerweg Braunschweiger Süden <https://www.pfarrverband-braunschweiger-sueden.de/pilgerweg/>`__ beim Pfarrverband Braunschweiger Süden
* `Artikel in der Braunschweiger Zeitung <https://www.braunschweiger-zeitung.de/braunschweig/article231227298/Kostenloses-W-Lan-rund-um-Kirchen-im-Braunschweiger-Sueden.html>`__
* `Artikel in regionalHeute <https://regionalheute.de/braunschweig/kirchen-im-braunschweiger-sueden-bieten-gratis-wlan-an-1609138508/>`__
* `Artikel bei der Ev.-luth. Propstei Braunschweig <https://www.propstei-braunschweig.de/aktuell/nachricht/Artikel/10546//1/Meldungen.html?no_cache=1>`__

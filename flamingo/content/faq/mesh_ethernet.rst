sort: 200
title: Mesh via Ethernet


Mesh via Ethernet
=================

Normalerweise verbinden sich Freifunkrouter über ein WiFi-adhoc-Netzwerk miteinander.
In bestimmten Situationen können Ethernetverbindungen die Performanz und Verfügbarkeit
des Freifunknetzwerks lokal verbessern.

Einige Anwendungsfälle, bei denen eine Vernetzung über ein Ethernetkabel sinnvoll sein kann:

* Über Kabel verbindbare Knoten meshen nicht oder nur unzureichend über WLAN.
* Die Situation vor Ort erlaubt es, eine Vernetzung über Kabel mit geringem
  Aufwand einzurichten.
* Ein Router mit einer leistungsstarken CPU soll die VPN-Verbindung zum Gateway
  für mehrere nahegelegene Freifunkrouter bereit stellen.

Für die Verkabelung bieten sich die Varianten Mesh-on-WAN und Mesh-onLAN an.
Der Unterschied hierbei liegt lediglich darin welche Ports am Router
verwendet werden.
Im folgenden werden Router mit aktueller
`Firmware <https://firmware.freifunk-bs.de/#stable>`__ vorausgesetzt.

Mesh-on-WAN
-----------

Mit dieser Methode werden Freifunkknoten über deren WAN-Buchsen vernetzt.
WLAN und Kabel werden hierbei zumeist parallel zum Meshen verwendet.
Welches Medium tatsächlich zur Datenübertragung genutzt wird, entscheidet der
Routing-Algorithmus.
Mesh-on-WAN und VPN-Uplink können gleichzeitig aktiviert werden.

Pros und Cons dieser Methode:

* Vergleichsweise einfache Konfiguration.
* Ein externer Switch ist notwendig. Eventuell vorhandende LAN-Buchsen am
  Freifunk-Router ersetzen nicht einen externen Switch.
* Ein DHCP-Server muss mit dem Switch verbunden sein.

Zunächst müssen die Router entsprechend konfiguriert werden.
Das kann entweder mit dem Webbrowser im Konfigurationsmodus oder über
SSH im Betriebsmodus geschehen.

Mesh-on-WAN im Konfigurationsmodus einrichten
---------------------------------------------

Auf der Konfigurationsseite zur Einrichtung des Routers ist im Expertenmodus
der Haken bei *Mesh auf dem WAN-Port aktivieren* zu setzen.

Mesh-on-WAN im Betriebsmodus einrichten
---------------------------------------

Nachdem eine `SSH-Verbindung </faq/ssh.html>`__ hergestellt ist, sind folgende
Zeilen einzugeben:

.. code-block:: none

 uci set network.mesh_wan.auto=1
 uci commit network

Mesh-on-WAN kann mit folgenden Befehlen wieder deaktiviert werden.

.. code-block:: none

 uci set network.mesh_wan.auto=0
 uci commit network

Mesh-on-WAN-Verkabelung
-----------------------

.. img:: mesh_ethernet.png

Die WAN-Buchsen der entsprechend konfigurierten Freifunkrouter werden über einen
Switch miteinander verbunden.
Vom Switch führt ein Kabel zu dem Heimrouter, der auch als DHCP-Server dient
und eine Verbindung zum Internet zur Verfügung stellt.
Ein Setup mit zwei Freifunkroutern (TL-WR1043NDv2, Nanostation M2) soll die
Vernetzung exemplarisch veranschaulichen:

Die Freifunkouter meshen hier über Kabel (blau) und über adhoc-WLAN (gelb).
Freifunk-Clients verbinden sich über WLAN (magenta).
Die LAN-Buchsen am Freifunkrouter können ebenfalls als Client-Verbindung über
Kabel (magenta) dienen. Der Heimrouter stellt einen Zugang zum Internet (schwarz)
und jeweils einen DHCP-Server für das Heimnetz (grün) und das Gastnetz (blau)
zur Verfügung.

Sollte ein VLAN-fähiger Smart-Switch in der Vernetzung involviert sein, sind
die vom Switch und Router verwendenten VLAN-ID zu überprüfen, damit es nicht zu
ungewollten Überschneidungen kommt.
So wird vom Smart-Switch die VLAN-ID 1 oft als Default- oder Management-VLAN
verwendet, welches möglicherweise auch im Router konfiguriert ist.
Beispielsweise werden 1 (LAN) und 2 (WAN) als
`VLAN-ID im TL-WR1043v2 <http://wiki.openwrt.org/toh/tp-link/tl-wr1043nd#switch_ports_for_vlans>`__
verwendet.

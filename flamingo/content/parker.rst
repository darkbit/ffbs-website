blog:
   - date: 2020-05-26
     title:  Project Parker - Öffentlicher Test
     body:   Freifunk Project Parker ist ein neuer Ansatz für die Funktionsweise eines Freifunk Netzes. Unser Projekt befindet sich jetzt in einem öffentlichen Test.
   - date: 2020-06-03
     title: Project Parker - Öffentlicher Test
     body:  Die Project Parker Webseite wurde neu strukturiert. Die Inhalte wurden klarer zwischen Anwender und Profis getrennt.
   - date: 2020-11-15
     title: Project Parker - Öffentlicher Test
     body:  Es gibt nun auch eine Firmware-Historie für das Projekt-Parker.


Freifunk Project Parker: Neue Technik für Freifunk
==================================================

.. ffbsbox::

   | **Schnellzugriff**:
   | `Statistiken <https://w.freifunk-bs.de/grafana/d/LqfM5XSWz/global?orgId=1>`__

Project Parker ist eine Initiative des Freifunk Braunschweig eine neue
Freifunk-Technik zu entwickeln.
Wir haben über die letzten Jahre stetig hieran gearbeitet und trauen uns nun mit
diesem Projekt an die Öffentlichkeit zu gehen.

Für den Freifunk Endanwender ändert sich hierbei nicht viel:
Projekt Parker setzt weiterhin auf Gluon.
Damit unterstützten wir weiterhin die von Freifunk gewohnte Hardware und an
der Installation ändert sich ebenfalls nicht viel.
Die großen Änderungen sind im Verborgenen geschehen.

Seit Ende 2021 basiert Freifunk Braunschweig auf dem Projekct Parker.

Auf dieser Seite wollen wir eine Übersicht für Anwender und Profis geben.

Mitmachen für Anwender
----------------------

Für Endanwender gibt es die folgenden Möglichkeiten mitzumachen:

Über Project Parker surfen
..........................

Dies ist der einfachste Weg mitzumachen.
Suche mit deinem Gerät nach dem WLAN **Freifunk** und verbinde dich mit diesem.
Im `Meshviewer </map/#!/de/map>`__ kannst du die Positionen vieler Router finden.

Außerdem kannst Du im Parker-Netz wie gewohnt alle anderen Knoten erreichen.


Eigene Router betreiben
.......................

Einen eigenen Knoten zu betreiben ist mit *Project Parker* genau so einfach, wie beim klassischen Freifunk:

1. Einen kompatiblen Router besorgen. Für *Project Parker* funktionieren alle Router, die auch für ein klassischen Gluon funktionieren.
2. Firmware herunterladen und Router flashen. Falls der Router vorher mit einem klassischen Gluon betrieben wurde setze bei der Installation die Einstellungen zurück.
3. Der Router bootet nun - wie gewohnt - in den Konfigurationsmodus. Hier kannst du wie gewohnt Einstellungen vornehmen.
4. Du brauchst am Ende des Einrichtens aber keinen Schlüssel bei uns einreichen. Dein Router funktioniert direkt!

Die folgenden Links enthalten weitere Informationen:

* Die Firmware findest du `hier </parker/firmware.html>`__
* Um das Netz im Blick zu behalten gibt es den `meshviewer </map>`__, sowie ein `Grafana </grafana>`__

Während des Beta-Betriebs haben die Entwickler Zugriff auf die Geräte.
Zusätzlich senden die Geräte ihre Logdaten an einen zentralen Logserver in unserem Netz.

Mitmachen für Profis
--------------------

Im klassischen Gluon-basierenden Freifunk wird in der Regel mit B.A.T.M.A.N. ein großes Layer-2-Netz simuliert.
Gerade in großen Netzen (ab einigen hundert Knoten bzw. Benutzern) führt dies zu Problemen.
Im klassischen `Gluon <https://github.com/freifunk-gluon/gluon>`__ gibt es daher Unterstützung für
`Domains <https://gluon.readthedocs.io/en/v2020.1.x/features/multidomain.html>`__.
Diese Teilen ein Freifunk-Netz in mehrere kleine Abschnitte.

Project Parker versucht diesen Problemen mit einem anderen Ansatz zu begegnen.
Wir setzen auf klassisches Routing.
Diese Änderung erlaubt uns auch auf `fastd <https://wiki.freifunk.net/Fastd>`__ zu verzichten und stattdessen
`Wireguard <https://www.wireguard.com/>`__ einzusetzen.
Project Parker setzt B.A.T.M.A.N. nur noch dort ein, wo es den größten Vorteil bringt: in lokalen *Meshes* zwischen mehreren Knoten.
Dabei ist es egal ob einer oder mehrere Knoten in einem Mesh einen Uplink (VPN) haben.

Die Entwicklung wurde in 2018 begonnen und durch eine Serie an `Blogposts <https://stratum0.org/blog/categories/freifunk/>`__, sowie einem `Vortrag auf dem 35C3 <https://media.ccc.de/v/35c3oio-69-project-parker-klassisches-routing-fr-freifunk>`__ begleitet.

Firmware selber bauen
.....................

Wer tiefer in die Materie einsteigen möchte, darf dies natürlich auch gerne tun.
Hier findest du die wichtigsten Informationen um tiefer in die Technik einzusteigen.

* Die Firmware zu *Project Pakrer* basiert auf Gluon.
  Wir pflegen daher unseren eigenen Fork von Gluon mit unseren Änderungen.
  Dieser befindet sich in unserem `Gitlab <https://gitli.stratum0.org/ffbs/ffbs-gluon/tree/master>`__.
  Branches mit **-next** enthalten unsere Änderungen.
  Der Basisname entspricht dabei dem Upstream-Branch, auf den wir aufgesetzt haben.
  Zum Zeitpunkt des Schreibens ist der aktuelle Branch **v2020.1.x-ffbs-next**.
* Die passende Site befindet sich ebenfalls im `Gitlab <https://gitli.stratum0.org/ffbs/ffbs-site/-/tree/v2020.1.x-ffbs-next>`__.
  Die **-next** -Branches gehören hier ebenfalls wieder zum *Project Parker*.
  Zum Zeitpunkt des Schreibens ist der aktuelle Branch **v2020.1.x-ffbs-next**.
* Wir haben zusätzlich einen Fork des Upstream-Package Repos - ebenfalls im `Gitlab <https://gitli.stratum0.org/ffbs/packages/-/tree/v2020.1.x-ffbs-next>`__.
  Die **-next** -Branches gehören hier ebenfalls wieder zum *Project Parker*.
  Zum Zeitpunkt des Schreibens ist der aktuelle Branch **v2020.1.x-ffbs-next**.
* Unsere eigenen Pakete haben wir unserem `Package-Repository <https://gitli.stratum0.org/ffbs/ffbs-packages/-/tree/next>`__.
  Hier liegen die passenden Pakete im **next** -Branch.

Die *Package*-Repositories werden bereits von unserem Gluon, bzw. dem der Site referenziert.
Diese müssen nicht besonders gecloned werden.
Darüber hinaus baut sich die *Project Parker* Firmware wie jedes andere Gluon.
Siehe dazu diese `Anleitung <https://gluon.readthedocs.io/en/v2020.1.x/user/getting_started.html>`__.

Firmware entwickeln
...................

Wenn Du in die *Project Parker* Entwicklung einsteigen möchtest melde Dich
bei uns im `IRC </irc.html>`__ oder auf unserer `Kontaktadresse </kontakt.html>`__.
Aktuell treffen wir uns unregelmäßig ein, bis zweimal im Monat um die Entwicklung voran zu bringen.
Diese Treffen sind eine gute Gelegenheit in die Materie einzusteigen.

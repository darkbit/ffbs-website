branches:
    - name: v2020.2.2.2-ffbs-next
      channel: parker_beta
      date: 2021-03-31
site: "https://gitli.stratum0.org/ffbs/ffbs-site/-/commit/0a6c96bd21dad577c87281659ed80cad77a73a85"
gluon: "https://gitli.stratum0.org/ffbs/ffbs-gluon/-/commit/04643981263c2b4d6ee8393b91e1229e8ec085e2"
rtd:   "https://gluon.readthedocs.io/en/v2020.2.2/"


v2020.2.2.2-ffbs-next
=====================

* Die Adresse des local-node wurde auf 172.16.127.1 geändert.
  Die Statusseite der Router ist ab diesem Release also unter dieser IP zu erreichen.
  Wir hoffen damit weniger Kollisionen mit bestehenden Netzen zu haben.
* Es wurde ein Fehler behoben, bei dem der IPv4-DHCP-Server nicht immer
  zuverlässig gestartet wurde.
  Das Problem ist immer dann aufgetreten, wenn es in einem Mesh mehrere Router mit Uplink
  gibt.

* Bekannte Fehler:
    * In großen Meshes tauchen manchmal nicht alle Knoten im Meshviewer auf.
      Die Knoten, die nicht auftauchen ändern sich dabei über die Zeit.
      Problem hierbei scheint mit der MTU des VPN zusammenzuhängen.
      Eigentlich sollten diese Pakete richtig fragmentiert werden - das passiert aber nicht.
    * Im Meshviewer ist die Angabe des ausgewählten Gateways nicht sinnvoll.

* Änderungen am Gluon ohne direkte Auswirkung für den Benutzer:
    * site.conf: prefix4 und prefix6 entfernt und im Gluon dahinter ausgebaut.
      Damit sollte die Route nach 2001:bf7:38ff::/64 auf br-client nicht mehr angelegt werden.
      Systeme, die vor dieser Version eingerichtet wurden haben diese Route weiterhin.
    * Damit ist auch eine nicht matchende Firefall-Regel für respondd weggefallen.
    * Nodeoute: Logging verbessert: Fehler bei Änderungen am UCI werden nun gefangen und ins Log geschrieben.


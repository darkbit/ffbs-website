branches:
    - name: v2020.2.1.1-ffbs-next-202011211314-parker_beta
      channel: parker_beta
      date: 2020-11-21
site: "https://gitli.stratum0.org/ffbs/ffbs-site/-/commit/c7f28de057878c5ec93d7aa19190e4f0fbf2068d"
gluon: "https://gitli.stratum0.org/ffbs/ffbs-gluon/-/commit/e652dec0a119e4554a9cc907d92ede6ee20bb833"
rtd:   "https://gluon.readthedocs.io/en/v2020.2.1/"


v2020.2.1.1-ffbs-next
=====================

* Der vorläufige Fix für die Anzahl der verbundenen Clients wurde
  durch eine bessere, langfristige Lösung ersetzt.
  Die Anzahl der verbundenen Clients sollte nun in allen Fällen
  korrekt gezählt werden. 
  Die Diskussion zu dieser Lösung kann `hier <https://github.com/freifunk-gluon/gluon/pull/2147>`__
  nachgelesen werden.
* Da wir mittlerweile dazu übergegangen sind regelmäßige Releases
  der Parker-Firmware zu machen haben wir uns entschlossen diesen
  *richtige* Namen zu geben, anstatt die von ``git`` autogenerierten
  zu verwenden.

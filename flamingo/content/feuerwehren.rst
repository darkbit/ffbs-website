blog:
   - date: 2020-06-03
     title:  Freifunk bei Feuerwehr in Braunschweig
     body:   Marco hat in ehrenamtlicher Arbeit die Gerätehäuser von über 30 Feuerwehren im Braunschweiger Stadtgebiet mit Freifunk versorgt. Hier erfährst Du seine Geschichte.


Freifunk bei Feuerwehr in Braunschweig
======================================

.. img::
   feuerwehren.jpg
   :align: right
   :width: 400px

   Auf diesem Foto ist, gut versteckt, ein installierter Freifunk-Router zu sehen.

Marco hat in ehrenamtlicher Arbeit die Gerätehäuser von über 30 Feuerwehren im
Braunschweiger Stadtgebiet mit Freifunk versorgt.
Auf unsere `Karte <https://freifunk-bs.de/map>`__ sind Knoten, die Marco
installiert hat (und damit auch die der Feuerwehren) am Namenszusatz
**freifunk.city** zu erkennen.

Die Feuerwehr muss für ihren Dienst DSL-Anschlüsse vorhalten.
Da ist es naheliegend diesen auch für die anwesenden Kameraden nutzbar zu
machen.
Nicht wenige verbringen schließlich einen beträchtlichen Teil ihrer Freizeit
in und um die Gerätehäuser.
Nach Rücksprache mit den Betreibern darf dieser Anschluss nun für Freifunk mitgenutzt werden,
so lang sichergestellt ist, dass der Betrieb hierdurch nicht gestört wird.

Die finanziellen Mittel von rund 3500 € wurden von der Stadt Braunschweig zur
Verfügung gestellt.
Dieses Projekt ist ein gutes Beispiel für die Zusammenarbeit der
Stadt Braunschweig mit dem Freifunk Braunschweig:
Da Freifunk mit seinem Dienst kein Geld verdienen möchte, ist es für
uns kein Problem Knoten außerhalb von Stadtzentren zu betreiben, wo nur
zeitweise jeweils einige Teilnehmer das Netz nutzen.

Ein Blick auf unsere Karte zeigt:
Die Router gliedern sich gut in unser Netz ein.
Viele befinden sich in den jeweiligen Zentren der Orte und Stadtteile.
Einige haben in ihrer Nachbarschaft auch bereits Knoten zum Meshen gefunden.

Weiterführende Links
....................

* Artikel über Freies WLAN in Braunschweig im `Innovationsportal <https://www.braunschweig.de/innovationsportal/smartes-stadtleben/wlan.php>`__ der Stadt Braunschweig
* Braunschweiger Zeitung: `Kostenloses WLAN in Leiferde und Stöckheim <https://www.braunschweiger-zeitung.de/braunschweig/article228277077/Kostenloses-WLAN-in-Leiferde-und-Stoeckheim.html>`__ (Paywall)

disabled: true
title: Nanostation Loco M2

{{Infobox Router
|Name = Nanostation Loco M2
|Bild = nanoloco_overview3.jpg
|Bildunterschrift = ''Nanostation Loco M2''
|Hersteller = Ubiquity
|Modell = Nanostation Loco M2
|2.4GHz = Ja
|5GHz = Nein
|Standards = IEEE 802.11n,g,b
|Antennen = 1 x 8dBi Gerichtet
|AbstrahlungHorizontal = ca. 60°
|AbstrahlungVertikal = ca. 60°
|Reichweite = ca. 100m
|Outdoor = Outdoor
|Stromversorgung = POE
|Durchsatz= ca. 6 Mbps
|Clients = ca. 15
|LANPorts = keiner
|Gesamtpreis= ca. 50 Eur
}}
Die Nanostation Loco besitzt eine interne Antenne welche in einem Winkel von ca. 60° Horizontal / 60° Vertikal abstrahlt und ist dementsprechend am Rand und oberhalb der zu versorgenden Stelle zu plazieren. Die Befestigung erfolgt an einem Masten oder einem Balkongeländer mittels der beigefügten Kabelbinder, oder aber mit einer zusätzlichen Halterung an der Wand / einem Masten.

<b>Achtung:</b> Die Nanostation wird in mehreren Varianten angeboten ! Als Nanostation, Nanostation Loco und jeweils in der Varianten M2 und M5. Die Nanostation (ohne Loco) unterscheidet sich von der Nanostation Loco durch eine deutlich gerichtetere Abstrahlung und ist somit mehr für gerichtete Verbindungen geeignet, die Loco leuchtet eine Fläche gleichmäßiger aus. Die Version M2 funkt im 2.4 Ghz Band, die Version M5 im 5GHz Band. Zur Versorgung mit Freifunk ist nur die M2 Version im 2.4GHz Band geeignet!

== Einsatzszenarien ==
Der Nanostation Loco M2 ist ein semiprofessionelles Gerät für den Innen- und Außenbereich und gut geeignet um kleinere Hallen, Strassenbereiche oder Plätze auszuleuchten. 

Der Router kann eine kleinere Anzahl Clients (ca. 15) mit einer Gesamtbandbreite von ca. 6MBit/s versorgen.

== Stromversorgung == 
Die Nanostation wird über ein abgesetztes 24V / 0,5A POE - Netzteil versorgt welches im Lieferumfang enthalten ist.

== Anschlüsse und Bedienelemente ==
Die Nanostation Loco besitzt lediglich einen WAN Port, dementsprechend sind Szenarien wie Mesh on LAN oder die zusätzliche Anbindung von PCs nicht möglich. 
In das zum heimischen Router führende Kabel wird das POE Netzteil zwischengeschaltet um die Nanostation mit Strom zu versorgen. Der mit "POE" bezeichnete Anschluß wird hierbei an die Nanostation angeschlossen, der mit "LAN" bezeichnete Anschluß an das heimische Netzwerk.

Sowohl an der Nanostation wie auch am POE - Netzteil befinden sich ein Reset - Taster welche bspw. mit einer Büroklammer betätigt werden können.

== Befestigung == 
Die Befestigung erfolgt an einem Masten oder einem Balkongeländer mittels der beigefügten Kabelbinder, hierbei ist dann allerdings die Abstrahlrichtung nicht beeinflußbar.

Mittels einer zusätzlichen Halterung, wie bspw. der "Nanobracket Universal" (ca. 6 Euro) läßt sich die Nanostation hingegen bis zu 80° ausrichten, und an einer Wand oder einem Masten befestigen. Weiterer Vorteil ist hierbei, daß das Gerät nicht über Kabelbinder sondern "massiv" befestigt wird.

== Versionsnummer ==
Derzeit gibt es nur eine Version der Nanostation Loco M2: V1.

== Bezugsquellen ==
* '''Nanostation Loco M2:'''
** [https://www.wecanhelp.de/422305002/productsearch?pr=NzU1NzkxNjIw&e=MDQyNTA2MDU1OTg4MjI=&searchterm=nanostation%20loco%202 Preisvergleich]
** [http://www.amazon.de/gp/offer-listing/B00CREZK2A/ref=sr_1_1_olp?ie=UTF8&qid=1434279275&sr=8-1&keywords=nanostation+loco+m2&condition=new&tag=boost86-21 Amazon]
* '''Nanobracket Universal:'''
** [http://varia-store.com/Zubehoer/Befestigungen/NanoBracket-Universal-Holder-for-NanoStation-other-CPEs::1228.html Varia-Store]

== Fotos ==

=== Nanostation Loco M2 ===
<gallery style="text-align:center" mode="packed">
Nanoloco package.jpg
Nanoloco_overview1.jpg | Frontansicht
Nanoloco_overview2.jpg | Rückansicht
Nanoloco_overview3.jpg | Anschlüsse
Nanoloco_connections.jpg | Anschlüsse
</gallery>

=== Nanostation POE Netzteil ===
<gallery style="text-align:center" mode="packed">
Nanoloco poe1.jpg
Nanoloco poe2.jpg
Nanoloco poe4.jpg
Nanoloco poe5.jpg
Nanoloco poe6.jpg
</gallery>

=== Nanostation Universal Bracket ===
<gallery style="text-align:center" mode="packed">
Nanoloco_bracket2.jpg
Nanoloco_bracket1.jpg
</gallery>
[[Category:Hardware]]

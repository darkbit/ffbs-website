template: page.html

blog:
    - date: 2020-03-22
      title: Wöchentliche Treffen finden erst einmal digital statt
      body:  Aufgrund der aktuellen Situation ist unser regulärer Treffpunkt im Stratum 0 zur Zeit geschlossen. Anstelle unserer Treffen vor Ort treffen wir uns nun Mittwochs ab 19:00 Uhr in einer Online Konferenz.
    - date: 2020-05-10
      title: Browserempfehlung zum Online-Meeting
      body:  Unsere Wöchentliche Online-Konferenz funktioniert am Besten in Chrome oder Chromium


Online Meetups
==============

Aufgrund der aktuellen Situation ist unser regulärer Treffpunkt im Stratum 0
zur Zeit geschlossen
(`Mehr Informationen <https://stratum0.org/blog/posts/2020/03/16/space-geschlossen/>`__).
Natürlich sind wir weiterhin auf unserer
`Mailingliste <http://lists.freifunk.net/mailman/listinfo/braunschweig-freifunk.net>`__
und auch im
`IRC </irc.html>`__ zu erreichen.

.. ffbsbox::

   | **Kurzlinks**:
   | `Videokonferenz im Browser <https://treffen.freifunk-bs.de/FreifunkBS>`__
   | `App für Android <https://play.google.com/store/apps/details?id=org.jitsi.meet>`__
   | `App für iOS <https://apps.apple.com/de/app/jitsi-meet/id1165103905>`__

Anstelle unserer Treffen vor Ort treffen wir uns nun Mittwochs ab 19:00 Uhr
in einer Online Konferenz.
Hier erhälst du weitere Informationen, wie du unserer Konferenz beitreten kannst.


Jitsi im Browser
----------------

Wenn du der Konferenz mit einem Browser auf einem Desktop-PC oder Notebook
beitreten möchtest kannst du einfach den folgenden Link benutzen:
`https://treffen.freifunk-bs.de/FreifunkBS <https://treffen.freifunk-bs.de/FreifunkBS>`__.

**Benutze wenn möglich Chrome oder Chromium.**
Andere Browser können zu Problemen führen.

Beim Betreten des Raumes musst du der Webseite in deinem Browser erlauben
dein Mikrofon zu verwenden.
Der Zugriff auf die Kamera ist natürlich optional.
Es bietet sich an ein Headset zu verwenden.
Hast du kein Headset zur Hand reichen in der Regel auch normale Kopfhörer und
die eingebauten Mikrofone eines Notebooks.

Jitsi auf dem Smartphone
------------------------

Es sind Apps für
`Android <https://play.google.com/store/apps/details?id=org.jitsi.meet>`__ und
`iOS <https://apps.apple.com/de/app/jitsi-meet/id1165103905>`__
verfügbar.

Installiere die App bevor du auf den Link *Videokonferenz im Browser* klickst.
Anschließend bietet Dir die Webseite an die Konferenz in der App zu öffnen.

Beim ersten öffnen der App musst du erlauben dein Mikrofon zu verwenden.
Der Zugriff auf die Kamera ist natürlich optional.

Es bietet sich an ein Headset zu verwenden.
Hast du kein Headset zur Hand reichen in der Regel auch normale Kopfhörer und
die eingebauten Mikrofone deines Smartphones.

Eigene Konferenzen
------------------

Unter `https://treffen.freifunk-bs.de <https://treffen.freifunk-bs.de>`__
kannst Du Konferenzen mit einem eigenen Namen starten.
Teile den Link anschließend mit den anderen Teilnehmern.

Dankeschön
----------

Ein großes Dankeschön geht an td00 von `https://jitsi.rocks <http://jitsi.rocks>`__
für die Bereitstellung und den Betrieb dieses großartigen Services!

from docutils.parsers.rst import directives
from docutils.nodes import raw

from flamingo.plugins.rst import NestedDirective

def ffbsbox(context):
    class Div(NestedDirective):
        def run(self):
            html = self.parse_content(context)

            return [
                raw('', '<div style="float:right; clear:right; width:20%; border-top:1px solid; border-bottom:1px solid; margin-left:2em; margin-top:1.5em; margin-bottom:1.5em;">{}</div>'.format(html), format='html'),
            ]

    return Div


class rstFfbsBox:
    def parser_setup(self, context):
        directives.register_directive('ffbsbox', ffbsbox(context))

from bs4 import BeautifulSoup
from flamingo.core.utils.html import extract_title

from flamingo.core.parser import ContentParser


class RawParser(ContentParser):
    FILE_EXTENSIONS = ['html']

    def parse(self, file_content, content):
        markup_string = self.parse_meta_data(file_content, content)

        soup = BeautifulSoup(markup_string, 'html.parser')
        title = extract_title(soup)

        content['content_title'] = title
        content['content_body'] = markup_string


class Raw:
    def parser_setup(self, context):
        context.parser.add_parser(RawParser(context))


$(function () {
    $.get('/s0_finanz.json', function (ans) {
        let types = [];
        let bound = 0;
        ans.appropriations.forEach(function (appr) {
            if (appr.subject.indexOf('Freifunk') > -1) {
                types = types.concat(appr.types);
                bound += parseFloat(appr.amount);
            }
        });
        $('#finanz_total').text(bound.toString());
        const typeIds = types.map(function (typ) {
            return ans.types.findIndex(function (t) { return t.indexOf(typ) > -1; });
        });
        const income = [];
        const expense = [];
        const labels = [];
        for (let i = 1; i <= 12; i++) {
            labels.unshift(ans.months[i].yearmonth);
            for (let j = 0; j < typeIds.length; j++) {
                ((j%2) === 0 ? income : expense).unshift(Math.abs(parseFloat(ans.months[i].typeTotals[typeIds[j]])));
            }
        }
        const ctx = document.getElementById('graph').getContext('2d');
        const chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels,
                datasets: [{
                    backgroundColor: '#90ee90',
                    label: 'Einnahmen',
                    data: income
                }, {
                    backgroundColor: '#ff6363',
                    label: 'Ausgaben',
                    data: expense
                }],
            },
            options: {
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'EUR',
                        },
                        ticks: {
                            beginAtZero: true,
                        },
                    }],
                },
                tooltips: {
                    callbacks: {
                        label: function (item) { return item.yLabel+'€'; },
                    },
                },
            },
        });
    });
});
